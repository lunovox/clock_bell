Bell Chimes Mod v1.0
By Leslie E. Krause

Bell Chimes adds a set of church bells that are chimed automatically at different periods 
to audibly denote the passage of time in world. The bells are non-craftable and intended
for placement by an administrator in a conspicuous location, like a bell tower at spawn.

 - At noon each day the half Westminster chimes are played.
 - At midnight each day the full Westminster chimes are played.
 - At midnight on the first day of a new month the full Westminster chimes are played
   followed by a succession of tolls for the current month.
 - At midnight on the first day of a new year the full Westminster chimes are played 
   followed by a 30-second carillon.

Generally speaking, if your time speed is set to the standard 20-minutes per in-game day, 
then you will hear the monthly tolls about twice per real-world day. Likewise, you will
hear the carillon chimes about six times per real-world month.

Both the great bell and quarter bells are sounded by means of a mechanical rack-and-snail 
actuator that strikes each bell with a hammer on the side. For the time being (no pun
intended), this is only a sound-effect, but in the next version a model of the physical 
device will be included. For now you, are welcome to construct the turret clock striking 
mechanism to scale for added realism in your world. Here are some useful references:

 *  How the Rack and Snail Bell Striking Mechanism on a Tower Clock Works
    https://www.youtube.com/watch?v=MOe5WthyTgA

 *  Lampasas County Courthouse Tower Clock
    https://www.youtube.com/watch?v=sx_w62HZf-E

Also included is an API for custom formatting of the in-game date and time without the 
need for cumbersome mathematical calculations. I published the source code on the forums 
a couple years ago, but I decided it might be more useful within a standalone mod.

Two helper functions are available for converting the time and date into strings. These 
can be useful in formspecs, chat commands, HUD elements, etc. Both accept a tokenized 
input string for customization.

 * minetest.get_date_string( str, env_date )
   Returns the game date as a human-readable string

    * str - an optional tokenized string to represent the game date
    * env_date - an optional game date specifier, or the current date if nil

   The tokenized string may include one or more date specifiers:

      Cardinal values:
      %Y - elapsed years in epoch
      %M - elapsed months in year
      %D - elapsed days in month
      %J - elapsed days in year

      Ordinal values:
      %y - current year of epoch
      %m - current month of year
      %d - current day of month
      %j - current day of year

 * minetest.get_time_string( str, env_time )
   Returns the game time as a human-readable string

    * str - an optional tokenized string to represent the game time
    * env_date - an optional game time specifier, or the current time if nil

   The tokenized string may include one or more time specifiers:
      %pp - daypart as AM or PM
      %hh - hours in 24-hour clock
      %cc - hours in 12-hour clock
      %mm - minutes

Even though there is no formal calendar convention in Minetest games, I settled on months 
having 30 days and years having 360 days -- a suitable homage to the geometric simplicity 
of the voxel world.

And lastly, there is a new chat command for obtaining the in-game date or time, as well 
as the server's local date and time. Simply type "/clock" followed by "date" or "time".


Repository
----------------------

Browse source code...
  https://bitbucket.org/sorcerykid/belfry

Download archive...
  https://bitbucket.org/sorcerykid/belfry/get/master.zip
  https://bitbucket.org/sorcerykid/belfry/get/master.tar.gz

Compatability
----------------------

Minetest 0.4.14+ required

Dependencies
----------------------

Basic Ownership Mod (required)
  https://bitbucket.org/sorcerykid/ownership

Installation
----------------------

  1) Unzip the archive into the mods directory of your game
  2) Rename the belfry-master directory to "beflry"

License of source code
----------------------------------------------------------

GNU Lesser General Public License v3 (LGPL-3.0)

Copyright (c) 2020, Leslie E. Krause (leslie@searstower.org)

This program is free software; you can redistribute it and/or modify it under the terms of
the GNU Lesser General Public License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Lesser General Public License for more details.

http://www.gnu.org/licenses/lgpl-2.1.html


Multimedia License (textures, sounds, and models)
----------------------------------------------------------

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)

   /textures/church_bell.png
   created by Sokomine
   obtained from https://github.com/FaceDeer/bell

   /sounds/bell_chime1.ogg
   created by theblockofsound235
   modified by sorcerykid
   obtained from https://freesound.org/people/theblockofsound235/sounds/458302/

   /sounds/bell_chime2.ogg
   created by theblockofsound235
   modified by sorcerykid
   obtained from https://freesound.org/people/theblockofsound235/sounds/458302/

   /sounds/bell_mechanism1.ogg
   created by crooner
   modified by sorcerykid
   obtained from https://freesound.org/people/crooner/sounds/42928

   /sounds/bell_mechanism2.ogg
   created by crooner
   modified by sorcerykid
   obtained from https://freesound.org/people/crooner/sounds/42928/

   /sounds/bell_mechanism3.ogg
   created by crooner
   modified by sorcerykid
   obtained from https://freesound.org/people/crooner/sounds/42928/

   /sounds/bell_mechanism5.ogg
   created by Towerclock1843
   modified by sorcerykid
   obtained from https://www.youtube.com/watch?v=vI1c0dXZS3g

   /sounds/bell_mechanism6.ogg
   created by Towerclock1843
   modified by sorcerykid
   obtained from https://www.youtube.com/watch?v=vI1c0dXZS3g

   /sounds/bell_mechanism7.ogg
   created by Towerclock1843
   modified by sorcerykid
   obtained from https://www.youtube.com/watch?v=vI1c0dXZS3g

   /sounds/bell_stroke.ogg
   created by edsward
   modified by sorcerykid
   obtained from https://freesound.org/people/edsward/sounds/341866/

You are free to:
Share — copy and redistribute the material in any medium or format.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and
indicate if changes were made. You may do so in any reasonable manner, but not in any way
that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that
legally restrict others from doing anything the license permits.

Notices:

You do not have to comply with the license for elements of the material in the public
domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary
for your intended use. For example, other rights such as publicity, privacy, or moral
rights may limit how you use the material.

For more details:
http://creativecommons.org/licenses/by-sa/3.0/
